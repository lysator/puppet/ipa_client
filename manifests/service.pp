# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include ipa_client::service
class ipa_client::service {
  require ipa_client::install
  service { 'sssd':
    ensure => running,
    enable => true,
  }
}
