# @summary Install packages
#
# @example
#   include ipa_client::install
class ipa_client::install (
  $packages = undef,
) {
  require ipa_client::keytab
  ensure_packages($packages)
}
