# Installs the krb5 keytab
class ipa_client::keytab {
  if ($::ipa_client::keytab_content != undef) {
    file { '/etc/krb5.keytab':
      ensure  => file,
      content => $::ipa_client::keytab_content,
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      notify  => [
        Service['sssd'],
      ],
    }
  } else {
    warning('Could not find keytab, please add it via hiera.')
  }
}
