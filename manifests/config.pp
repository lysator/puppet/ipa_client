# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include ipa_client::config
class ipa_client::config (
  String $ldap_config_file,
) {
  require ::ipa_client::install

  # this should really be configurable
  $basedn = 'dc=ad,dc=lysator,dc=liu,dc=se'

  # Configure sssd
  $ipa_servers = join($ipa_client::servers, ', ')
  $sssd_base_services = ['nss', 'pam', 'ssh']

  if ($::ipa_client::use_sudo) {
    $sudo_service = ['sudo']
    $sudo_config = {
      'sudo' => {}
    }
  } else {
    $sudo_service = []
    $sudo_config = {}
  }

  if ($::ipa_client::use_autofs) {
    $autofs_service = ['autofs']
    $autofs_config = {
      'autofs' => {}
    }
  } else {
    $autofs_service = []
    $autofs_config = {}
  }

  $sssd_services = $sssd_base_services + $sudo_service + $autofs_service
  $config = {
    sssd => {
      services => join($sssd_services, ', '),
      domains => $ipa_client::domain,
    },
    "domain/${ipa_client::domain}" => {
      id_provider => 'ipa',
      dns_discovery_domain => $ipa_client::domain,
      ipa_server => "_srv_, ${ipa_servers}",
      ipa_domain => $ipa_client::domain,
      ipa_hostname => $facts['networking']['fqdn'],
      auth_provider => 'ipa',
      chpass_provider => 'ipa',
      cache_credentials => false,
      ldap_tls_cacert => '/etc/ipa/ca.crt',
      krb5_store_password_if_offline => false,
      enumerate => false,
      debug_level => 5,
    },
  } + $sudo_config + $autofs_config

  file { '/etc/sssd/sssd.conf':
    ensure  => file,
    mode    => '0700',
    content => hash2ini($config, {quote_char => ''}),
    notify  => Service['sssd'],
  }

  # And kerberos
  $krb5_realm = upcase($::ipa_client::domain)
  $krb5_kcd = $::ipa_client::servers[0]
  $krb5_conf = @("EOT")
    includedir /etc/krb5.conf.d/
    includedir /var/lib/sss/pubconf/krb5.include.d/

    [libdefaults]
      default_realm = ${krb5_realm}
      dns_lookup_realm = false
      dns_lookup_kdc = false
      rdns = false
      dns_canonicalize_hostname = false
      ticket_lifetime = 24h
      forwardable = true
      udp_preference_limit = 0
      default_ccache_name = KEYRING:persistent:%{uid}


    [realms]
      ${krb5_realm} = {
        pkinit_anchors = FILE:/var/lib/ipa-client/pki/kdc-ca-bundle.pem
        pkinit_pool = FILE:/var/lib/ipa-client/pki/ca-bundle.pem
        kdc = ${krb5_kcd}
      }


    [domain_realm]
      .${ipa_client::domain} = ${krb5_realm}
      ${ipa_client::domain} = ${krb5_realm}
      ${facts['networking']['fqdn']} = ${krb5_realm}
      .${facts['networking']['domain']} = ${krb5_realm}
      ${facts['networking']['domain']} = ${krb5_realm}
    | EOT
  # IMPORTANT: Do NOT change the indentation of the line above - that WILL break the IPA client
  # (the includedir directives and section headers MUST NOT be indented in the resulting file)

  file { '/etc/krb5.conf':
    ensure  => file,
    mode    => '0644',
    content => $krb5_conf,
    notify  => Service['sssd'],
  }

  # Only use the two aes-sha1 enctypes for maximum compatibility. SHA1
  # is deprecated, but should still be fine in HMACs. Add more types
  # in the future when compatibility is better.
  $krb5_crypto_policies = @("EOT")
    [libdefaults]
    permitted_enctypes = aes256-cts-hmac-sha1-96 aes128-cts-hmac-sha1-96
    | EOT

  file { '/etc/krb5.conf.d/crypto-policies':
    ensure  => file,
    mode    => '0644',
    content => $krb5_crypto_policies,
    notify  => Service['gssproxy'],
  }

  # IPA client config
  file { '/etc/ipa':
    ensure => directory,
    mode   => '0755'
  }
  file { '/etc/ipa/default.conf':
    ensure  => file,
    mode    => '0644',
    content => hash2ini({
      'global' => {
        'basedn'     => $basedn,
        'realm'      => upcase($::ipa_client::domain),
        'domain'     => $::ipa_client::domain,
        'server'     => $::ipa_client::servers[0],
        'host'       => $::fqdn,
        'xmlrpc_uri' => "https://${::ipa_client::servers[0]}/ipa/xml",
        'enable_ra'  => 'True',
      },
    }, { quote_char        => '', key_val_separator => ' = ', }),
    require => File['/etc/ipa'],
  }
  file { '/etc/ipa/ca.crt':
    ensure  => file,
    mode    => '0644',
    source  => 'puppet:///modules/ipa_client/ca.crt',
    require => File['/etc/ipa'],
  }

  # ldap config
  file_line { "${ldap_config_file} URI":
    path  => $ldap_config_file,
    line  => "URI ldaps://${::ipa_client::servers[0]}",
    match => '^URI ',
  }
  file_line { "${ldap_config_file} BASE":
    path  => $ldap_config_file,
    line  => "BASE ${basedn}",
    match => '^BASE ',
  }
  file_line { "${ldap_config_file} TLS_CACERT":
    path  => $ldap_config_file,
    line  => 'TLS_CACERT /etc/ipa/ca.crt',
    match => '^TLS_CACERT ',
  }
  file_line { "${ldap_config_file} SASL_MECH":
    path  => $ldap_config_file,
    line  => 'SASL_MECH GSSAPI',
    match => '^SASL_MECH ',
  }

  # Disable socket-activated units for services configured in sssd.conf
  # Keeping them enabled produces errors:
  # sssd_check_socket_activated_responders[666]: (2023-01-15 21:40:39:049124): [sssd] [main] (0x0070): Misconfiguration found for the nss responder.
  # sssd_check_socket_activated_responders[666]: The nss responder has been configured to be socket-activated but it's still mentioned in the services' line in /etc/sssd/sssd.conf.
  # sssd_check_socket_activated_responders[666]: Please, consider either adjusting your services' line in /etc/sssd/sssd.conf or disabling the nss's socket by calling:
  # sssd_check_socket_activated_responders[667]: "systemctl disable sssd-pam.socket"
  $sssd_services.each |$service| {
    service { "sssd-${service}.socket":
      enable => false
    }
  }

  if $facts['os']['family'] == 'Debian' {
    ensure_packages(['gssproxy'], {
      ensure => present,
      before => Service['gssproxy'],
    })
  }

  service { 'gssproxy':
    enable => true,
  }
}
