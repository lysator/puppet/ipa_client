class ipa_client::autofs (
  $packages = undef,
  $service = undef,
){
  include ::ipa_client::service
  ensure_packages($packages, { notify => Service[$service], })

  service { $service:
    ensure    => $::ipa_client::use_autofs,
    enable    => $::ipa_client::use_autofs,
    subscribe => Service['sssd'],
  }
}
