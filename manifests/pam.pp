# Setup PAM to use FreeIPA
class ipa_client::pam (
  $files = {},
  $base_dir = '/etc/pam.d/',
) {
  $files.each | $filename, $content | {
    file { "${base_dir}/${filename}":
      ensure  => file,
      content => $content,
      mode    => '0644',
      notify  => Service['sssd'],
    }
  }
}
