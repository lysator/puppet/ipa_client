# Setup a FreeIPA-client
# Will use
class ipa_client (
  String $domain = undef,
  Array[String] $servers = undef,
  Optional[String] $keytab_content = undef,
  Boolean $use_autofs = true,
  Boolean $use_sudo = true,
  Boolean $manage_autofs = true,
  Boolean $manage_pam = true,
){
  require ::ipa_client::keytab
  include ::ipa_client::config
  include ::ipa_client::service
  include ::ipa_client::nsswitch
  if ($manage_autofs) {
    include ::ipa_client::autofs
  }
  if ($manage_pam) {
    include ::ipa_client::pam
  }
}
