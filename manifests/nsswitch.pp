class ipa_client::nsswitch (
  $conf = undef,
){
#  if ($facts['os']['family'] == 'RedHat') {
#    file { '/etc/authselect/user-nsswitch.conf':
#      ensure  => file,
#      content => $conf,
#      mode    => '0644',
#    }
#    exec { '/usr/bin/authselect select sssd':
#      subscribe   => File['/etc/authselect/user-nsswitch.conf'],
#      refreshonly => true,
#    }
#    -> exec { '/usr/bin/authselect apply-changes --force':
#      subscribe   => File['/etc/authselect/user-nsswitch.conf'],
#      refreshonly => true,
#    }
#  } else {
    file { '/etc/nsswitch.conf':
      ensure  => file,
      content => $conf,
      mode    => '0644',
    }
#  }
}
